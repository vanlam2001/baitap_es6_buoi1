const DTB = (...e) =>{
     let sum = 0;
     e.forEach(elem => {
        sum += parseFloat(elem);
     });
     return (sum / e.length).toFixed(2);
};

function getElementById(id){
    let dom = document.getElementById(id)
    return dom;
}

getElementById("btnKhoi1").onclick = function(){
    let e = getElementById("inpToan").value,
        n = getElementById("inpLy").value,
        t = getElementById("inpHoa").value;
    getElementById("tbKhoi1").innerHTML = DTB(e,n,t)
}

getElementById("btnKhoi2").onclick = function(){
    let e = getElementById("inpVan").value,
        n = getElementById("inpSu").value,
        t = getElementById("inpDia").value;
    getElementById("tbKhoi2").innerHTML = DTB(e,n,t)
}












