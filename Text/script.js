const heading = document.querySelector(".heading");

const addSpanTags = (e) => {
    return [...e].map(char => `<span>${char}</span>`).join("");
}

heading.innerHTML =  addSpanTags(heading.innerText);