const danhSachMau = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

let container = document.getElementById("colorContainer");

const loadBoChonMau = () => {
  danhSachMau.forEach((color, index) => {
    let buttonClass = " color-button " + color;
    if(index === 0){
      buttonClass += " active ";
    }
    container.innerHTML += `<button class="${buttonClass}"></button>`;
  });
};

loadBoChonMau();


let mauButton = document.getElementById("color-button");
let ngoiNha = document.getElementById("house");


let boChonMau = document.getElementsByClassName("color-button"),
    house = document.getElementById("house");
for (let o = 0; o < boChonMau.length; o++) boChonMau[o].addEventListener("click", function() {
    chinhSuaMau(danhSachMau[o], o)
});
chinhSuaMau = ((o, e) => {
    for (let o = 0; o < boChonMau.length; o++) boChonMau[o].classList.remove("active");
    boChonMau[e].classList.add("active"), house.className = "house " + o
});


